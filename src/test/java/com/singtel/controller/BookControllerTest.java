package com.singtel.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.singtel.domain.Book;
import com.singtel.service.BookService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.*;

import java.util.Optional;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class BookControllerTest {

    private MockMvc mockMvc;

    @Mock
    private BookService bookService;

    @InjectMocks
    private BookController bookController;

    private static final String BASE_URL_STR = "/api/books/";

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(bookController)
                .build();
    }


    @Test
    public void shouldReturnCreateWhenSuccessfullyPostTheBook() throws Exception {
        Book book = new Book();
        book.setId(2);
        book.setTitle("Java API Development");

        when(bookService.insert(any())).thenReturn(Optional.of(book));
        mockMvc.perform(
                post(BASE_URL_STR)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(book)))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", containsString(BASE_URL_STR + book.getId())));

        verify(bookService, times(1)).insert(any());
        verifyNoMoreInteractions(bookService);
    }

    @Test
    public void shouldReturnNoContentWhenFailingToInsert() throws Exception {
        Book book = new Book();
        book.setId(2);
        book.setTitle("Java API Development");

        when(bookService.insert(any())).thenReturn(Optional.empty());
        mockMvc.perform(
                post(BASE_URL_STR)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(book)))
                .andExpect(status().isNoContent());

        verify(bookService, times(1)).insert(any());
        verifyNoMoreInteractions(bookService);
    }

    @Test
    public void shouldReturnBadRequestWhenPOSTWithInvalidPayload() throws Exception {
        Book book = new Book();
        book.setId(2);
        book.setTitle("Java API Development");

        when(bookService.insert(any())).thenReturn(Optional.of(book));
        mockMvc.perform(
                post(BASE_URL_STR)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(""))
                .andExpect(status().isBadRequest());

        verify(bookService, times(0)).insert(any());
        verifyNoMoreInteractions(bookService);
    }

    @Test
    public void shouldReturnTheBookWhenProvidingAValidBookId() throws Exception {
        Book book = new Book();
        book.setId(2);
        book.setTitle("Java API Development");
        when(bookService.getBookById(book.getId())).thenReturn(Optional.of(book));
        mockMvc.perform(get(BASE_URL_STR + book.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is(book.getTitle())))
                .andExpect(jsonPath("$.id", is(book.getId())));
    }

    @Test
    public void shouldReturnNotFoundWhenProvidingInValidBookId() throws Exception {
        when(bookService.getBookById(1)).thenReturn(Optional.empty());
        mockMvc.perform(get(BASE_URL_STR + "1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
        verify(bookService, times(0)).insert(any());
    }

    @Test
    public void shouldReturnBadRequestWhenProvidingInvalidIdType() throws Exception {
        when(bookService.getBookById(1)).thenReturn(Optional.empty());
        mockMvc.perform(get(BASE_URL_STR + "xy")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
        verify(bookService, times(0)).insert(any());
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}