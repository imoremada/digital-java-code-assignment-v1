package com.singtel.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author indunil
 * <p>
 * This holds the author's details
 */
@Getter
@Setter
public class Author {
    private String name;
    private String description;
    private String qualifications;
    private List<String> previousPublications;

}
