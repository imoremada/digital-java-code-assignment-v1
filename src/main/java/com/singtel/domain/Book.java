package com.singtel.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author indunil
 *
 * This is an domain class to represent Book
 */
@Getter
@Setter
public class Book {
    private int id;
    private String title;
}
