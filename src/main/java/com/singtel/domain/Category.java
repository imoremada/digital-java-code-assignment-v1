package com.singtel.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author indunil
 * <p>
 * This holds book category details
 */
@Getter
@Setter
public class Category {
    private int categoryId;
    private String type;
}
