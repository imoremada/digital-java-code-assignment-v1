package com.singtel.controller;

import com.singtel.domain.Book;
import com.singtel.service.BookService;
import com.singtel.exceptions.BookNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Optional;

/**
 * @author indunil
 * <p>
 * This handles all the REST end points for Book domain
 */
@RestController
@RequestMapping("/api")
public class BookController {

    @Autowired
    private BookService bookService;

    private String baseUrlStr = "/api/books/";

    /**
     * Insert the given payload to the database. Returns a result
     * with NO_CONTENT if the insertion is failed
     *
     * @param book
     * @return
     */
    @PostMapping("/books")
    public ResponseEntity<?> insertBook(@RequestBody Book book) {
        Optional<Book> optionalBook = bookService.insert(book);
        if (optionalBook.isPresent()) {
            return ResponseEntity.created(URI.create(baseUrlStr +
                    optionalBook.get().getId())).body(optionalBook.get());
        }
        return ResponseEntity.noContent().build();
    }

    /**
     * Returns the book with the given id. BookNotFoundException
     * is thrown if the given id is not presented in the database
     *
     * @param bookId
     * @return
     */
    @GetMapping("/books/{bookId}")
    public ResponseEntity<?> retrieveBookById(@PathVariable int bookId) {
        Optional<Book> optionalBook = bookService.getBookById(bookId);
        if (optionalBook.isPresent()) {
            return ResponseEntity.ok().body(optionalBook.get());
        }
        throw new BookNotFoundException("Requested book is not available, bookId = " + bookId);
    }
}
