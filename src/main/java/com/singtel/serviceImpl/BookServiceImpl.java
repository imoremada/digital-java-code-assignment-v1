package com.singtel.serviceImpl;

import com.singtel.domain.Book;
import com.singtel.service.BookService;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author indunil
 * Service implementaion for the book domain class
 */
@Service
public class BookServiceImpl implements BookService {

    private static final String DEFAULT_TITLE = "Default Book Title";

    /**
     * Returns the same book instance (to simulate the behaviour of the database insertion result)
     *
     * @param book
     * @return
     */
    @Override
    public Optional<Book> insert(Book book) {
        return Optional.of(book);
    }

    /**
     * Returns a book with the given id (to simulate the behaviour of database get)
     *
     * @param bookId
     * @return
     */
    @Override
    public Optional<Book> getBookById(int bookId) {
        Book book = new Book();
        book.setTitle(DEFAULT_TITLE);
        book.setId(bookId);
        return Optional.of(book);
    }
}
