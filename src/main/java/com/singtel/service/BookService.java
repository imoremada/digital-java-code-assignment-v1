package com.singtel.service;

import com.singtel.domain.Book;

import java.util.Optional;

/**
 * @author indunil
 */
public interface BookService {

    Optional<Book> insert(Book book);

    Optional<Book> getBookById(int bookId);
}
