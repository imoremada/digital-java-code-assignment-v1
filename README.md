INSTRUCTIONS TO BUILD AND RUN

This is a maven project written in Java8.

1) Load as a maven project
2) Build the project
3) Run the main class com.singtel.App

Commands

1) POST command to be executed

Unix    - curl -d '{"id":1,"title":"API Development"}' -H 'Content-Type: application/json' http://localhost:8080/api/books
Windows - curl -d "{\"id\":1,\"title\":\"REST API Development\"}" -H "Content-Type: application/json" http://localhost:8080/api/books

2) GET command to be executed
curl -v http://localhost:8080/api/books/1


Assumptions 
1) This is just REST POST and GET implementation with unit tests for the Controller
2) In the service creating new instances as an when needed (Since no interaction with the databases to simulate the database resulting behaviour)



